(defproject clubot "0.1.0-SNAPSHOT"
  :dependencies [[clj-redis "0.0.12"]
                 [environ "1.0.0"]
                 [irclj "0.5.0-alpha4"]
                 [org.apache.logging.log4j/log4j-core "2.0.2"]
                 [org.apache.logging.log4j/log4j-jcl "2.0.2"]
                 [org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.logging "0.3.0"]]
  :description "A customizable life embetterment robot, with parentheses."
  :license {:name "The MIT License"
            :url "http://opensource.org/licenses/mit"}
  :main ^:skip-aot evil-robot-ted.main
  :min-lein-version "2.4.2"
  :plugins [[codox "0.8.10"]]
  :profiles {:uberjar {:aot :all}}
  :target-path "target/%s"
  :uberjar-name "evil-robot-ted.jar"
  :url "https://bitbucket.org/zobar/clubot")
