(ns clubot.core
  (:require [clubot.adapter.core :as adapter]
            [clubot.robot :as bot]))

(defn- require-and-resolve
  [name]
  (let [ns (symbol (namespace name))]
    (require ns)
    (resolve name)))

(defn- configure
  [[name config]]
  ((require-and-resolve name) config))

(defn run
  "Creates an adapter and robot from the given configuration, and runs clubot."
  [{:keys [adapters plugins] :as config}]
  (let [adapter (adapter/group (map configure adapters))
        plugin (apply bot/plugin (map require-and-resolve plugins))
        robot (plugin config)]
    (adapter robot)))
