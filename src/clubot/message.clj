(ns clubot.message
  (:refer-clojure :exclude [send]))

(defn- type?
  [type]
  (fn [message]
    (= type (:type message))))

(defn emote
  [message & strings]
  ((:emote message) strings))

(defn enter
  [properties]
  (assoc properties :type :enter))

(def enter? (type? :enter))

(defn leave
  [properties]
  (assoc properties :type :leave))

(def leave? (type? :leave))

(defn reply
  [message & strings]
  ((:reply message) strings))

(defn send
  [message & strings]
  ((:send message) strings))

(defn text
  [properties]
  (assoc properties :type :text))

(def text? (type? :text))

(defn topic
  [properties]
  (assoc properties :type :topic))

(def topic? (type? :topic))
