(ns clubot.plugins.example
  (:require [clubot.message :as msg]
            [clubot.robot :as bot]))

(defn- salutation
  [event replies]
  (event (fn [message]
    (when (:user message)
      (msg/send message (rand-nth replies))))))

;
; A plugin is a function which takes a configuration and returns a robot.
;
(defn plugin
  [config]

  (bot/robot config

    ;
    ; The bot/hear callback is called any time a message's text matches. For
    ; example:
    ;
    ; * Stop badgering the witness
    ; * badger me
    ; * what exactly is a badger anyways
    ;
    ; msg/send sends a message exactly as specified regardless of who said it,
    ; "Badgers? BADGERS? WE DON'T NEED NO STINKIN BADGERS".
    ;
    (bot/hear #"(?i:badger)"
      [message _]
      (msg/send message "Badgers? BADGERS? WE DON'T NEED NO STINKIN BADGERS"))

    ;
    ; The bot/respond callback is only called for messages that are preceded by
    ; any of the robot's names. The regular expression you provide with the
    ; callback is matched against the portion of the message following the
    ; robot's name, after any intervening whitespace has been stripped. If the
    ; robot's names are HAL and /, then this callback would be triggered for:
    ;
    ; * hal open the pod bay doors
    ; * HAL: open the pod bay doors
    ; * @HAL open the pod bay doors
    ; * /open the pod bay doors
    ;
    ; It wouldn't be called for:
    ;
    ; * HAL: please open the pod bay doors
    ;   - because the regular expression is anchored at the beginning
    ; * has anyone ever mentioned how lovely you are when you open pod bay doors?
    ;   - because it lacks the robot's name
    ;
    ; The second callback parameter is the result of matching the incoming
    ; message against the regular expression. This is the result of
    ; clojure.core/re-find, which ends up being a vector with index 0 being the
    ; full text matching the expression. If you include capture groups, those
    ; will be populated starting at index 1. Here, we use destructuring syntax
    ; to extract the door type from the regular expression match.
    ;
    ; If a user Dave says "HAL: open the pod bay doors", msg/reply sends a
    ; message "Dave: I'm afraid I can't let you do that."
    ;
    (bot/respond #"\A(?i:open the (.*) doors)"
      [message [_ door-type]]
      (msg/reply message
                 (if (= "pod bay" door-type)
                   "I'm afraid I can't let you do that."
                   (str "Opening " door-type " doors"))))

    ;
    ; msg/emote emotes a message to the room.
    ;
    (bot/hear #"(?i:i like pie)"
      [message _]
      (msg/emote message "makes a freshly baked pie"))

    ;
    ; clubot can react to a room's topic changing.
    ;
    (bot/topic
      [message]
      (msg/send message (str (:text message) "? That's a Paddlin'")))

    ;
    ; clubot can see users entering and leaving.
    ;
    ; Normally you will use the macros enter, hear, leave, respond, and topic to
    ; declare your callbacks. clubot also exposes the raw fns that the macros
    ; compile down to (enter*, hear*, leave*, respond*, and topic*), should you
    ; need to use them. Here we pass the enter* and leave* fns into a
    ; higher-order function called salutation, which registers a callback that
    ; responds with a random message.
    ;
    (salutation bot/enter*
      ["Hi" "Target Acquired" "Firing" "Hello friend." "Gotcha" "I see you"])

    (salutation bot/leave*
      ["Are you still there?" "Target lost" "Searching"])))
