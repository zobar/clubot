(ns clubot.plugins.logger
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log]
            [clubot.robot :as bot]))

(defn- room-handler
  [event]
  (fn [{:keys [room user]}]
    (if user
      (log/info event room user)
      (log/info event room))))

(defn- text-handler
  [event]
  (fn [{:keys [room text user]}]
    (let [user (str user \:)]
      (if room
        (log/info event room user text)
        (log/info event user text)))))

(defn plugin
  [config]
  (bot/robot config
    (bot/enter* (room-handler "ENTER"))
    (bot/leave* (room-handler "LEAVE"))
    (bot/text* (text-handler "TEXT"))
    (bot/topic* (text-handler "TOPIC"))))
