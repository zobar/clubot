(ns clubot.adapter.core)

(defn group
  [adapters]
  #(doseq [adapter adapters] (adapter %)))
