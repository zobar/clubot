(ns clubot.adapter.irc
  (:refer-clojure :exclude [send])
  (:require [clubot.message :as msg]
            [irclj.core :as irc]))

(defn- send-with
  [sender]
  (fn [strings] (doseq [string strings] (sender string))))

(defn- emote
  [connection room]
  (send-with (partial irc/ctcp connection room :action)))

(defn- reply
  [connection room user]
  (send-with #(irc/message connection room (str user ": " %))))

(defn- send
  [connection room]
  (send-with (partial irc/message connection room)))

(defn- channel-props
  ([connection room]
    {:emote (emote connection room)
     :room room
     :send (send connection room)})
  ([connection room user]
    (assoc (channel-props connection room)
           :reply (reply connection room user)
           :user user)))

(defn- text-props
  ([connection user text]
    (dissoc (text-props connection user user text) :room))
  ([connection room user text]
    (assoc (channel-props connection room user)
           :text text)))

(defn adapter
  [{:keys [channels host nick port]}]
  (fn [dispatch]
    (let [channel-callback
            (fn [event]
              (fn [connection {[room] :params user :nick}]
                (dispatch (event (if (= nick user)
                                     (channel-props connection room)
                                     (channel-props connection room user))))))

          text-callback
            (fn [event]
              (fn [connection {[room text] :params user :nick}]
                (dispatch (event (if (= nick room)
                                     (text-props connection user text)
                                     (text-props connection room user text))))))

          callbacks
            {:join (channel-callback msg/enter)
             :part (channel-callback msg/leave)
             :privmsg (text-callback msg/text)
             :topic (text-callback msg/topic)}

          connection (irc/connect host port nick :callbacks callbacks)]

      (doseq [channel channels] (irc/join connection channel)))))
