(ns clubot.robot
  (:import [java.util.regex Pattern])
  (:require [clojure.string :as str]
            [clubot.message :as msg]))

(defmacro ^:private defhandler
  [name bindings & body]
  (let [fn-name (symbol (str name \*))
        params (butlast bindings)]
  `(do
    (defn ~fn-name ~bindings ~@body)
    (defmacro ~name
      [~@params bindings# & body#]
      `(~~fn-name ~~@params (fn ~bindings# ~@body#))))))

(defn- predicate-handler
  [predicate callback]
  #(when (predicate %) (callback %)))

(defmacro ^:private defpredhandler
  [name predicate]
  `(defhandler ~name [callback#]
    (constantly (predicate-handler ~predicate callback#))))

(defn- regexp-handler
  [get-text regexp callback]
  (fn [message]
    (when (msg/text? message)
      (when-let [text (get-text message)]
        (when-let [match (re-find regexp text)]
          (callback message match))))))

(defpredhandler enter msg/enter?)

(defhandler hear
  [regexp callback]
  (constantly (regexp-handler :text regexp callback)))

(defpredhandler leave msg/leave?)

(defn robot
  "A robot is a function which accepts a message and acts on it. This function
  applies a configuration to a set of plugins to return a robot that handles all
  of them."
  [config & plugins]
  (let [listeners (map #(% config) plugins)]
    #(doseq [listener listeners] (listener %))))

(defn plugin
  "A plugin is a function which takes a configuration and returns a robot. This
  function builds a larger plugin from a set of smaller plugins."
  [& plugins]
  (fn [config]
    (apply robot config plugins)))

(defhandler respond
  [regexp callback]
  (fn [config]
    (let [quoted-names (str/join \| (map #(Pattern/quote %) (:names config)))
          user-regexp (re-pattern (str "\\A@?(?i:" quoted-names ")[,:]?\\s*(.*)\\z"))]
      (regexp-handler #(get (re-find user-regexp (:text %)) 1)
                      regexp callback))))

(defpredhandler text msg/text?)

(defpredhandler topic msg/topic?)
