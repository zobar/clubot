(ns clubot-memo
  (:import [java.text DateFormat]
           [java.util Date])
  (:require [clj-redis.client :as redis]
            [clubot.message :as msg]
            [clubot.robot :as bot]))

(defn- memo
  [from body]
  (let [now (Date.)
        date (.format (DateFormat/getDateInstance) now)
        time (.format (DateFormat/getTimeInstance DateFormat/SHORT) now)]
    (str "On " date " at " time ", " from " said: " body)))

(defn- redis-key
  [room user]
  (str "clubot-memo:" room \: user))

(defn plugin
  [{redis-config :redis :as config}]

  (let [redis (redis/init redis-config)

        notify-on
          (fn [event]
            (event (fn [{:keys [room user] :as message}]
              (when user
                (let [key (redis-key room user)
                      memos (redis/lrange redis key 0 -1)]
                  (when (seq memos)
                    (apply msg/reply message memos)
                    (redis/del redis [key])))))))]

    (bot/robot config

      (notify-on bot/enter*)
      (notify-on bot/text*)

      (bot/respond #"(?i:memo\s+to\s+([^\s:]+):?\s+(.*))"
        [{from :user :keys [room] :as message} [_ to body]]
        (let [memo (memo from body)]
          (redis/rpush redis (redis-key room to) memo)
          (msg/reply message "👍"))))))
