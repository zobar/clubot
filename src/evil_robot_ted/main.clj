(ns evil-robot-ted.main
  (:gen-class)
  (:require [clubot.core :as clubot]
            [environ.core :refer [env]]))

(def ^:private config
  {:adapters '{clubot.adapter.irc/adapter {:channels ["#irc_driven_development"]
                                           :host "chat.freenode.net"
                                           :nick "evil_robot_ted"
                                           :port 6665}}
   :names ["!" "evil_robot_ted"]
   :plugins '[;clubot.plugins.example/plugin
              clubot.plugins.logger/plugin
              clubot-memo/plugin]
   :redis {:url (env :rediscloud-url)}})

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (clubot/run config))
